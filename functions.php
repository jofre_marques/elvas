<?php
/**
 * elvas functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package elvas
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function elvas_setup() {
	/*
		* Make theme available for translation.
		* Translations can be filed in the /languages/ directory.
		* If you're building a theme based on elvas, use a find and replace
		* to change 'elvas' to the name of your theme in all the template files.
		*/
	load_theme_textdomain( 'elvas', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
	add_theme_support( 'title-tag' );

	/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		*/
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus(
		array(
			'menu-1' => esc_html__( 'Primary', 'elvas' ),
		)
	);

	/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'style',
			'script',
		)
	);

	// Set up the WordPress core custom background feature.
	add_theme_support(
		'custom-background',
		apply_filters(
			'elvas_custom_background_args',
			array(
				'default-color' => 'ffffff',
				'default-image' => '',
			)
		)
	);

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support(
		'custom-logo',
		array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		)
	);
}
add_action( 'after_setup_theme', 'elvas_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function elvas_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'elvas_content_width', 640 );
}
add_action( 'after_setup_theme', 'elvas_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function elvas_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'elvas' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'elvas' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'elvas_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function elvas_scripts() {
	wp_enqueue_style( 'elvas-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'elvas-style', 'rtl', 'replace' );

	wp_enqueue_script( 'elvas-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'elvas_scripts' );



/**
 *  INCLUDES FROM SCRIPTS & STYLES
 */

function internit_theme_scripts(){

	wp_enqueue_style('internit-theme-style', get_template_directory_uri().'/assets/dist/css/template-globals.css');

	wp_enqueue_style('internit-theme-style-global', get_template_directory_uri().'/style.css');

	wp_enqueue_script('internit-theme-bootstrap-js', get_template_directory_uri().'/assets/dist/js/libary/bootstrap/bootstrap.bundle.min.js', array('jquery'), '5.1.3',  true);

	wp_enqueue_script('internit-theme-bootstrap-js', get_template_directory_uri().'/assets/dist/js/libary/bootstrap/bootstrap.min.js', array('jquery'), '5.1.3',  true);

	wp_enqueue_script('internit-theme-lightbox-js', get_template_directory_uri().'/assets/dist/js/libary/lightbox/fslightbox.min.js', false, '3.0.2 ',  true);

	wp_enqueue_script('internit-theme-mask-js', get_template_directory_uri().'/assets/dist/js/libary/mask/jquery.mask.min.js', array('jquery'), '1.14.16',  true);

	wp_enqueue_script('internit-theme-inputTel-js', get_template_directory_uri().'/assets/dist/js/libary/tellinput/intlTelInput.min.js', array('jquery'), '17.0.12');

	wp_enqueue_script('internit-theme-swiperjs', get_template_directory_uri().'/assets/dist/js/libary/swiper/swiper-bundle.min.js', array('jquery'), '6.5.6',  true);

	wp_enqueue_script('internit-storage-js', get_template_directory_uri().'/assets/dist/js/libary/storage/storage.js', false, '1.0', true);

	wp_enqueue_script('internit-theme-js', get_template_directory_uri().'/assets/dist/js/template-globals.js', false, '1.0', true);
}

add_action('wp_enqueue_scripts', 'internit_theme_scripts');



//INCLUINDO O TYPE MODULO
function add_type_attribute($tag, $handle) {
    // if not your script, do nothing and return original $tag
    if ( 'internit-theme-js' !== $handle) {
        return $tag;
    }
    // change the script tag by adding type="module" and return it.
    $tag = '<script type="module" src="' . get_template_directory_uri().'/assets/dist/js/template-globals.js"></script>';
    return $tag;
}
add_filter('script_loader_tag', 'add_type_attribute' , 10, 3);

// Post Type Post
function custom_post_type_blog(){
	register_post_type('blog', array(
		'label' => 'Blog',
		'description' => 'Blog',
		'menu_position' => 5,
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'capability_type' => 'post',
		'map_meta_cap' => true,
		'hierarchical' => false,
		'query_var' => true,
		'supports' => array('title', 'editor'),

		'labels' => array(
			'name' => 'Blog',
			'singular_name' => 'Post',
			'menu_name' => 'Blog',
			'add_new' => 'Adicionar Post',
			'add_new_item' => 'Adicionar Post',
			'edit' => 'Editar',
			'edit_item' => 'Editar Post',
			'new_item' => 'Novo Post',
			'view' => 'Ver Post',
			'view_item' => 'Ver Post',
			'search_items' => 'Procurar Post',
			'not_found' => 'Nenhum Post encontrado',
			'not_found_in_trash' => 'Nenhum Post encontrado na lixeira',
		),
	));
}

add_action('init', 'custom_post_type_blog');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}
/**
 * Post Empreendimento
 */

function custom_post_type_empreendimento(){        // Mude o nome da função para o modelo de post que deseja criar, EX.: custom_post_type_livraria()
	register_post_type('empreendimento', array(    //1* Modifique a string "blog" para o nome do modelo de post que deseja criar. Ex.: 'livraria'
		'label' => 'Empreendimento',               // O título da aba que vai aparecer no painel admin
		'description' => 'Empreendimento',         // A descrição desse tipo de post.
		'menu_position' => 5,            // A posição em que ele vai estar com base nos outros elementos. Leia mais na documentação.
		'public' => true,                // Mantenha "true" como padrão. Leia a documentação para mais informações
		'show_ui' => true,               // Mantenha "true" como padrão. Leia a documentação para mais informações
		'show_in_menu' => true,          // Mantenha "true" como padrão. Leia a documentação para mais informações
		'capability_type' => 'post',     // Mantenha "true" como padrão. Leia a documentação para mais informações
		'map_meta_cap' => true,          // Mantenha "true" como padrão. Leia a documentação para mais informações
		'hierarchical' => false,         // Mantenha "false" como padrão. Leia a documentação para mais informações
		'query_var' => true,             // Mantenha "true" como padrão. Leia a documentação para mais informações
		'supports' => array('title', 'editor'), // Os recursos que serão disponíveis dentro dessa área de edição.

		'labels' => array( //Abaixo são os rotulos das áreas padrões de um tipo "post". Apenas substitua ao seu gosto.
			'name' => 'Empreendimentos',
			'singular_name' => 'Empreendimento',
			'menu_name' => 'Empreendimentos',
			'add_new' => 'Adicionar Empreendimento',
			'add_new_item' => 'Adicionar Empreendimento',
			'edit' => 'Editar',
			'edit_item' => 'Editar Empreendimento',
			'new_item' => 'Novo Empreendimento',
			'view' => 'Ver Empreendimento',
			'view_item' => 'Ver Empreendimento',
			'search_items' => 'Procurar Empreendimento',
			'not_found' => 'Nenhum Empreendimento encontrado',
			'not_found_in_trash' => 'Nenhum Empreendimento encontrado na lixeira',
		),
	));
}

add_action('init', 'custom_post_type_empreendimento'); //Adicionando sua ação as execuções do wordpress. 