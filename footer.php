<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package elvas
 */

?>

<?php
get_template_part('template-parts/globals/content', 'footer');
?>

<?php if (is_page('Portal Cliente')) : ?>
	<footer id="colophon" class="site-footer">
		<div class="footer-internit">
			<a href="https://internit.com.br" target="_blank">
				<img src="<?= get_template_directory_uri() ?>/assets/src/img/logos/logo-internit.png" alt="" width="100%">
			</a>
		</div>
	</footer><!-- #colophon -->
<?php endif; ?>
</div><!-- #page -->

<toolbar>
	<div class="toolbar_content">
		<div class="toolbar_box">
			<div class="toolbar_box-item">
				<div class="toolbar_box-item-text">
					<span>Compre seu imóvel</span>
				</div>
			</div>
			<div class="toolbar_box-item">
				<div class="toolbar_box-item-icon">
					<a href="">
						<img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/tel-branco.png" alt="Icon">
					</a>
				</div>
			</div>
			<div class="toolbar_box-item">
				<div class="toolbar_box-item-icon">
					<a href="">
						<img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/wpp.png" alt="Icon">
					</a>
				</div>
			</div>
			<div class="toolbar_box-item">
				<div class="toolbar_box-item-icon">
					<a href="">
						<img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/email.png" alt="Icon">
					</a>
				</div>
			</div>
			<div class="toolbar_box-item">
				<div class="toolbar_box-item-icon">
					<img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/person.png" alt="Icon">
				</div>
			</div>
			<div class="toolbar_box-item">
				<div class="toolbar_box-item-icon">
					<img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/tel-branco.png" alt="Icon">
				</div>
				<div class="toolbar_box-item-text">
					<span>Elvas <br> te liga!</span>
				</div>
			</div>
		</div>
	</div>
</toolbar>


<div id="router" data-url="<?= get_template_directory_uri() ?>"></div>

<?php wp_footer(); ?>

</body>

</html>