<?php

/**
 * Template Name: Empreendimento
 */

get_header();
?>
<section class="page-empreendimento">
    <div class="container-fluid elvas">
        <div class="row elvas_content">
            <div class="col-12 elvas_content-collum px-1 px-md-3">
                <div class="container">
                    <div class="row elvas_development">
                        <div class="col-12 elvas_development-collum text-center px-1 px-md-3">
                            <div class="elvas_development-title">
                                <h1><?php the_field('nome_do_empreendimento') ?></h1>
                            </div>
                            <div class="elvas_development-logo-empreendimento text-center">
                                <div class="elvas_development-logo-empreendimento-img">
                                    <img src="<?= get_field('logo_empreendimento')['sizes']['medium'] ?>" alt="" style="width: 100%;">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="page-empreendimento">
    <div class="container-fluid elvas">
        <div class="row elvas_content">
            <div class="col-12 elvas_content-collum px-1 px-md-3">
                <div class="container">
                    <div class="row elvas_development">
                        <?php $galeriaMain = get_field('galeria_principal');
                        if (!empty($galeriaMain)) : ?>
                            <div class="col-12 col-md-8 elvas_development-collum px-1 px-md-3">
                                <div class="elvas_development-list">
                                    <div class="elvas_development-list--item">
                                        <div class="elvas_development-box">
                                            <div class="elvas_development-box--item">
                                                <div class="elvas_development-box--carousel-vertical">
                                                    <div class="swiper-container js-thumbnail-galeria-empreendimento">
                                                        <!-- Wrapper adicional necessário -->
                                                        <div class="swiper-wrapper">
                                                            <!-- Slides -->
                                                            <?php


                                                            foreach ($galeriaMain as $img) :  ?>
                                                                <div class="swiper-slide">
                                                                    <div class="elvas_development-box--carousel-vertical-img">
                                                                        <img src="<?php echo esc_url($img['sizes']['large']); ?>" alt="<?php echo esc_attr($img['alt']); ?>">
                                                                    </div>
                                                                </div>
                                                            <?php endforeach;
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="elvas_development-box--item">
                                                <div class="elvas_development-box--carousel-horizontal">
                                                    <div class="swiper-container js-galeria-empreendimento">
                                                        <!-- Wrapper adicional necessário -->
                                                        <div class="swiper-wrapper">
                                                            <!-- Slides -->
                                                            <?php


                                                            foreach ($galeriaMain as $img) :  ?>
                                                                <div class="swiper-slide">
                                                                    <a data-fslightbox="gallery" href="<?php echo esc_url($im['url']); ?>">
                                                                        <img src="<?php echo esc_url($img['sizes']['large']); ?>" alt="<?php echo esc_attr($img['alt']); ?>">
                                                                    </a>
                                                                </div>
                                                            <?php endforeach;
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="col-12 col-md-4 elvas_development-collum px-1 px-md-3">
                            <div class="elvas_development--descricao">
                                <div class="elvas_development--descricao-title">
                                    <h4><strong>Descrição</strong></h4>
                                </div>
                                <div class="elvas_development--descricao-text">
                                    <p><?php the_field('descricao_do_empreendimento') ?></p>
                                </div>
                            </div>
                            <div class="elvas_development--detalhes">
                                <div class="elvas_development--detalhes-title">
                                    <p><strong>Detalhes da unidade</strong></p>
                                </div>
                                <div class="elvas_development--detalhes-info-area d-flex">
                                    <div class="elvas_development--detalhes-info-area-1">
                                        <div class="elvas_development--detalhes-info d-flex">
                                            <div class="elvas_development--detalhes-info-icon">
                                                <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/medida.png" alt="" width="100%">
                                            </div>
                                            <div class="elvas_development--detalhes-info-text">
                                                <p><?php the_field('metros_quadrados') ?></p>
                                            </div>
                                        </div>
                                        <div class="elvas_development--detalhes-info d-flex mt-2">
                                            <div class="elvas_development--detalhes-info-icon">
                                                <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/double-bed-yellow.png" alt="" width="100%">
                                            </div>
                                            <div class="elvas_development--detalhes-info-text">
                                                <p><?php the_field('quartos') ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="elvas_development--detalhes-info-area-2">
                                        <div class="elvas_development--detalhes-info d-flex">
                                            <div class="elvas_development--detalhes-info-icon">
                                                <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/chuveiro.png" alt="" width="100%">
                                            </div>
                                            <div class="elvas_development--detalhes-info-text">
                                                <p><?php the_field('banheiros') ?></p>
                                            </div>
                                        </div>
                                        <div class="elvas_development--detalhes-info d-flex mt-2">
                                            <div class="elvas_development--detalhes-info-icon">
                                                <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/car.png" alt="" width="100%">
                                            </div>
                                            <div class="elvas_development--detalhes-info-text">
                                                <p><?php the_field('vagas_na_garagem') ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elvas_development--buttons">
                                <?php if (!empty(get_field('link_hostsite'))) : ?>
                                    <div class="elvas_development--buttons-item">
                                        <a href="<?php the_field('link_hostsite') ?>" target="_blank">Acesse o Hotsite</a>
                                    </div>
                                <?php endif;
                                if (!empty(get_field('link_mais_informacoes'))) : ?>
                                    <div class="elvas_development--buttons-item">
                                        <a href="<?php the_field('link_mais_informacoes') ?>" target="_blank">Mais informações</a>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="page-empreendimento">
    <div class="container-fluid elvas">
        <div class="row elvas_content">
            <?php if (!empty(get_field('link_do_tuor_virtual'))) : ?>
                <div class="col-12 elvas_content-collum px-1 px-md-3">
                    <div class="container">
                        <div class="row elvas_development">
                            <div class="col-12 elvas_development-collum px-1 px-md-3 text-center my-5">
                                <div class="elvas_development-text mb-4">
                                    <h2>Tour Virtual</h2>
                                </div>
                                <div class="elvas_development-video">
                                    <iframe src="<?php the_field('link_do_tuor_virtual') ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>

<section class="page-empreendimento">
    <div class="container-fluid elvas">
        <div class="row elvas_content">
            <div class="col-12 elvas_content-collum px-1 px-md-3">
                <div class="container">
                    <div class="row elvas_development">
                        <div class="col-12 elvas_development-collum px-1 px-md-3">
                            <div class="elvas_development-abas">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <?php if (!empty(get_field('decorado'))) : ?>
                                        <li class="nav-item">
                                            <a class="nav-link active " id="decorado-tab" data-toggle="tab" href="#decorado" role="tab" aria-controls="decorado" aria-selected="false">Decorado</a>
                                        </li>
                                    <?php endif; ?>
                                    <?php if (!empty(get_field('perspectivas'))) : ?>
                                        <li class="nav-item">
                                            <a class="nav-link" id="perspectivas-tab" data-toggle="tab" href="#perfil" role="tab" aria-controls="perspectivas" aria-selected="false">Perspectivas</a>
                                        </li>
                                    <?php endif; ?>
                                    <?php if (!empty(get_field('lazer'))) : ?>
                                        <li class="nav-item">
                                            <a class="nav-link" id="lazer-tab" data-toggle="tab" href="#contato" role="tab" aria-controls="lazer" aria-selected="false">Lazer</a>
                                        </li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                            <div class="elvas_development-block">
                                <div class="tab-content" id="myTabContent">
                                    <?php if (!empty(get_field('decorado'))) : ?>
                                        <div class="tab-pane fade active show " id="decorado" role="tabpanel" aria-labelledby="decorado-tab">
                                            <div class="elvas_development-box">
                                                <div class="elvas_development-box--item">
                                                    <div class="elvas_development-box--carousel-horizontal">

                                                        <div class="swiper-container js-decorado-empreendimento">
                                                            <!-- Wrapper adicional necessário -->
                                                            <div class="swiper-wrapper">
                                                                <!-- Slides -->
                                                                <?php
                                                                if (have_rows('decorado')) :
                                                                    while (have_rows('decorado')) : the_row();  ?>
                                                                        <div class="swiper-slide">
                                                                            <div class="elvas_development-box--carousel-img">
                                                                                <a data-fslightbox="galleryDecorado" href="<?= get_sub_field('imagem_decorado')['url']; ?>">
                                                                                    <img src="<?= get_sub_field('imagem_decorado')['sizes']['large']; ?>" alt="<?= get_sub_field('imagem_decorado')['alt']; ?>">
                                                                                </a>
                                                                                <span><?php the_sub_field('texto_decorado'); ?></span>
                                                                            </div>
                                                                        </div>
                                                                <?php endwhile;
                                                                endif; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elvas_development-box--item">
                                                    <div class="elvas_development-box--carousel-vertical">
                                                        <div class="swiper-container js-thumbnail-decorado-empreendimento">
                                                            <!-- Wrapper adicional necessário -->
                                                            <div class="swiper-wrapper">
                                                                <!-- Slides -->
                                                                <?php
                                                                if (have_rows('decorado')) :
                                                                    while (have_rows('decorado')) : the_row();  ?>
                                                                        <div class="swiper-slide">
                                                                            <div class="elvas_development-box--carousel-thumbnail">
                                                                                <img src="<?= get_sub_field('imagem_decorado')['sizes']['large']; ?>" alt="<?= get_sub_field('imagem_decorado')['alt']; ?>">
                                                                            </div>
                                                                        </div>
                                                                <?php endwhile;
                                                                endif; ?>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Caso precise de navegação por botões -->
                                            <div class="swiper-thumbnail-decorado-button-prev">
                                                <img src="<?= get_template_directory_uri() ?>/assets/src/img/arrow-left.png" alt="Icon arrow">
                                            </div>
                                            <div class="swiper-thumbnail-decorado-button-next">
                                                <img src="<?= get_template_directory_uri() ?>/assets/src/img/arrow-right.png" alt="Icon arrow">
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (!empty(get_field('perspectivas'))) : ?>
                                        <div class="tab-pane fade" id="perfil" role="tabpanel" aria-labelledby="perspectivas-tab">
                                            <div class="elvas_development-box">
                                                <div class="elvas_development-box--item">
                                                    <div class="elvas_development-box--carousel-horizontal">
                                                        <div class="swiper-container js-perspectiva-empreendimento">
                                                            <!-- Wrapper adicional necessário -->
                                                            <div class="swiper-wrapper">
                                                                <!-- Slides -->
                                                                <?php
                                                                if (have_rows('perspectivas')) :
                                                                    while (have_rows('perspectivas')) : the_row();  ?>
                                                                        <div class="swiper-slide">
                                                                            <div class="elvas_development-box--carousel-img">
                                                                                <a data-fslightbox="galleryPerspectivas" href="<?= get_sub_field('imagem_perspectiva')['url']; ?>">
                                                                                    <img src="<?= get_sub_field('imagem_perspectiva')['sizes']['large']; ?>" alt="<?= get_sub_field('imagem_perspectiva')['alt']; ?>">
                                                                                </a>
                                                                                <span><?php the_sub_field('texto_perspectiva'); ?></span>
                                                                            </div>
                                                                        </div>
                                                                <?php endwhile;
                                                                endif; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elvas_development-box--item">
                                                    <div class="elvas_development-box--carousel-vertical">
                                                        <div class="swiper-container js-thumbnail-perspectiva-empreendimento">
                                                            <!-- Wrapper adicional necessário -->
                                                            <div class="swiper-wrapper">
                                                                <!-- Slides -->
                                                                <?php
                                                                if (have_rows('perspectivas')) :
                                                                    while (have_rows('perspectivas')) : the_row();  ?>
                                                                        <div class="swiper-slide">
                                                                            <div class="elvas_development-box--carousel-thumbnail">
                                                                                <img src="<?= get_sub_field('imagem_perspectiva')['sizes']['large']; ?>" alt="<?= get_sub_field('imagem_perspectiva')['alt']; ?>">
                                                                            </div>
                                                                        </div>
                                                                <?php endwhile;
                                                                endif; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Caso precise de navegação por botões -->
                                            <div class="swiper-thumbnail-perspectiva-button-prev">
                                                <img src="<?= get_template_directory_uri() ?>/assets/src/img/arrow-left.png" alt="Icon arrow">
                                            </div>
                                            <div class="swiper-thumbnail-perspectiva-button-next">
                                                <img src="<?= get_template_directory_uri() ?>/assets/src/img/arrow-right.png" alt="Icon arrow">
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (!empty(get_field('lazer'))) : ?>
                                        <div class="tab-pane fade" id="contato" role="tabpanel" aria-labelledby="lazer-tab">
                                            <div class="elvas_development-box">
                                                <div class="elvas_development-box--item">
                                                    <div class="elvas_development-box--carousel-horizontal">
                                                        <div class="swiper-container js-lazer-cempreendimento">
                                                            <!-- Wrapper adicional necessário -->
                                                            <div class="swiper-wrapper">
                                                                <!-- Slides -->
                                                                <?php
                                                                if (have_rows('lazer')) :
                                                                    while (have_rows('lazer')) : the_row();  ?>
                                                                        <div class="swiper-slide">
                                                                            <div class="elvas_development-box--carousel-img">
                                                                                <a data-fslightbox="galleryLazer" href="<?= get_sub_field('imagem_lazer')['url']; ?>">
                                                                                    <img src="<?= get_sub_field('imagem_lazer')['sizes']['large']; ?>" alt="<?= get_sub_field('imagem_lazer')['alt']; ?>">
                                                                                </a>
                                                                                <span><?php the_sub_field('texto_lazer'); ?></span>
                                                                            </div>
                                                                        </div>
                                                                <?php endwhile;
                                                                endif; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elvas_development-box--item">
                                                    <div class="elvas_development-box--carousel-vertical">
                                                        <div class="swiper-container js-thumbnail-lazer-empreendimento">
                                                            <!-- Wrapper adicional necessário -->
                                                            <div class="swiper-wrapper">
                                                                <!-- Slides -->
                                                                <?php
                                                                if (have_rows('lazer')) :
                                                                    while (have_rows('lazer')) : the_row();  ?>
                                                                        <div class="swiper-slide">
                                                                            <div class="elvas_development-box--carousel-thumbnail">
                                                                                <img src="<?= get_sub_field('imagem_lazer')['sizes']['large']; ?>" alt="<?= get_sub_field('imagem_lazer')['alt']; ?>">
                                                                            </div>
                                                                        </div>
                                                                <?php endwhile;
                                                                endif; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Caso precise de navegação por botões -->
                                            <div class="swiper-thumbnail-lazer-button-prev">
                                                <img src="<?= get_template_directory_uri() ?>/assets/src/img/arrow-left.png" alt="Icon arrow">
                                            </div>
                                            <div class="swiper-thumbnail-lazer-button-next">
                                                <img src="<?= get_template_directory_uri() ?>/assets/src/img/arrow-right.png" alt="Icon arrow">
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="page-empreendimento">
    <div class="container-fluid elvas">
        <div class="row elvas_content">
            <?php if (have_rows('galeria_de_plantas')) : ?>
                <div class="col-12 elvas_content-collum px-1 px-md-3">
                    <div class="container">
                        <div class="row elvas_development">
                            <div class="col-12 elvas_development-collum px-1 px-md-3 text-center">
                                <div class="elvas_development-title">
                                    <h2>Projetos que cativam</h2>
                                </div>
                            </div>
                        </div>
                        <div class="row elvas_development">
                            <div class="col-12 elvas_development-collum px-1 px-md-3">
                                <div class="elvas_development-carrousel-plants">
                                    <div class="swiper-container js-plants-empreendimento">
                                        <!-- Wrapper adicional necessário -->
                                        <div class="swiper-wrapper">
                                            <!-- Slides -->
                                            <?php

                                            while (have_rows('galeria_de_plantas')) : the_row();  ?>
                                                <div class="swiper-slide">
                                                    <div class="elvas_development-box">
                                                        <div class="elvas_development-box--item">
                                                            <a data-fslightbox="galleryLazer" href="<?= get_sub_field('imagem_plantas')['url']; ?>">
                                                                <img src="<?= get_sub_field('imagem_plantas')['sizes']['large']; ?>" alt="<?= get_sub_field('imagem_plantas')['alt']; ?>">
                                                            </a>
                                                        </div>
                                                        <div class="elvas_development-box--item">
                                                            <div class="elvas_development-group ">
                                                                <div class="elvas_development-group--planta">
                                                                    <div class="elvas_development-group--planta-title">
                                                                        <h5><?= get_sub_field('titulo_plantas') ?></h5>
                                                                    </div>
                                                                    <div class="elvas_development-group--planta-info">
                                                                        <ul>
                                                                            <?php if (have_rows('lista_de_itens_plantas')) :
                                                                                while (have_rows('lista_de_itens_plantas')) : the_row(); ?>
                                                                                    <li>
                                                                                        <div class="elvas_development-group--planta-info-list d-flex justify-content-between">
                                                                                            <div class="elvas_development-group--planta-info-text">
                                                                                                <p><?php the_sub_field('indice_plantas') ?></p>
                                                                                            </div>
                                                                                            <div class="elvas_development-group--planta-info-text">
                                                                                                <p><?php the_sub_field('valor_plantas') ?></p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </li>
                                                                            <?php
                                                                                endwhile;
                                                                            endif;
                                                                            ?>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endwhile;
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="elvas_development-below ">
                                    <div class="elvas_development-below--item">
                                        <div class="elvas_development-below--carousel-vertical">
                                            <div class="swiper-container js-thumbnail-plants-empreendimento">
                                                <!-- Wrapper adicional necessário -->
                                                <div class="swiper-wrapper">
                                                    <?php
                                                    if (have_rows('galeria_de_plantas')) :
                                                        while (have_rows('galeria_de_plantas')) : the_row();  ?>
                                                            <div class="swiper-slide">
                                                                <div class="elvas_development-box--carousel-thumbnail">
                                                                    <img src="<?= get_sub_field('imagem_plantas')['sizes']['large']; ?>" alt="<?= get_sub_field('imagem_plantas')['alt']; ?>">
                                                                </div>
                                                            </div>
                                                    <?php endwhile;
                                                    endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="elvas_development-below--item">
                                        <a href="<?php the_field('mais_informacoes') ?>">Solicite informações</a>
                                    </div>
                                </div>

                                <!-- Caso precise de navegação por botões -->
                                <div class="swiper-thumbnail-plants-button-prev">
                                    <img src="<?= get_template_directory_uri() ?>/assets/src/img/arrow-left.png" alt="Icon arrow">
                                </div>
                                <div class="swiper-thumbnail-plants-button-next">
                                    <img src="<?= get_template_directory_uri() ?>/assets/src/img/arrow-right.png" alt="Icon arrow">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>

<section class="page-empreendimento">
    <div class="container-fluid elvas">
        <div class="row elvas_content">
            <?php if (have_rows('informacao')) : ?>
                <div class="col-12 elvas_content-collum px-1 px-md-3">
                    <div class="container">
                        <div class="row elvas_development">
                            <div class="col-12 elvas_development-collum px-1 px-md-3 text-center">
                                <div class="elvas_development-title">
                                    <h2>Informações do Empreendimento</h2>
                                </div>
                            </div>
                        </div>
                        <div class="row elvas_development">
                            <div class="col-12 elvas_development-collum px-1 px-md-3">
                                <div class="elvas_development-box ">
                                    <?php
                                    while (have_rows('informacao')) : the_row(); ?>
                                        <div class="elvas_development-box--item">
                                            <div class="elvas_development-box--item-top">
                                                <div class="elvas_development-box--item-icon">
                                                    <img src="<?php the_sub_field('icone') ?>" alt="" width="100%">
                                                </div>
                                                <div class="elvas_development-box--item-info">
                                                    <div class="elvas_development-box--item-info-title">
                                                        <h6><strong><?php the_sub_field('titulo') ?></strong></h6>
                                                    </div>
                                                    <div class="elvas_development-box--item-info-text">
                                                        <p><?php the_sub_field('descricao') ?></p>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    <?php
                                    endwhile;
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row elvas_development">
                            <div class="col-12 elvas_development-collum px-1 px-md-3">
                                <div class="elvas_development-numbers">
                                    <div class="elvas_development-box--item-info-bottom">
                                        <div class="elvas_development-box--item-info-list d-flex justify-content-between">
                                            <ul>
                                                <?php $nu = 0;
                                                if (have_rows('lista_numerada')) :

                                                    while (have_rows('lista_numerada')) : the_row();

                                                        if ($nu > 0 && $nu % 3 == 0) :  ?>
                                            </ul>
                                            <ul>
                                                <li><?php echo "0" . ($nu + 1) . " - ";
                                                            the_sub_field('texto_numerado') ?></li>
                                            <?php else :      ?>
                                                <li><?php echo "0" . ($nu + 1) . " - ";
                                                            the_sub_field('texto_numerado') ?></li>
                                    <?php

                                                        endif;
                                                        $nu++;
                                                    endwhile;
                                                endif; ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <?php if (!empty(get_field('link_ebook_empreendimento'))) : ?>
                                    <div class="col-12 elvas_development-collum px-1 px-md-3">
                                        <div class="elvas_development-box justify-content-center">
                                            <div class="elvas_development-box-item">
                                                <div class="elvas_development-box--button">
                                                    <a href="<?php the_field('link_ebook_empreendimento') ?>">Baixe o Book do empreendimento</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
</section>
<section class="page-empreendimento">
    <div class="container-fluid elvas">
        <div class="row elvas_content">
            <?php if (!empty(get_field('porcentagem_da_obra'))) : ?>
                <div class="col-12 elvas_content-collum px-1 px-md-3">
                    <div class="container">
                        <div class="row elvas_development">
                            <div class="col-12 elvas_development-collum px-1 px-md-3 text-center">
                                <div class="elvas_development-title">
                                    <h2>Andamento da obra</h2>
                                </div>
                            </div>
                        </div>
                        <div class="row elvas_development justify-content-center mt-4">
                            <div class="col-12 col-lg-10 elvas_development-collum px-1 px-md-3">
                                <div class="elvas_development-box d-flex justify-content-between">

                                    <div class="elvas_development-box--item">
                                        <div class="elvas_development-box--item-info-icon" style="background-color: #cea547;">
                                            <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/mart.png" alt="" width="100%">
                                        </div>
                                    </div>
                                    <div class="elvas_development-box--item">
                                        <div class="elvas_development-box--item-info-progress js-bar">
                                            <span data-progress="<?php the_field('porcentagem_da_obra') ?>"></span>
                                            <span class="position-relative" data-barvalue>00%</span>
                                        </div>
                                    </div>
                                    <div class="elvas_development-box--item">
                                        <div class="elvas_development-box--item-info-icon" style="background-color: #cea547;">
                                            <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/key.png" alt="" width="100%">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row elvas_development">
                            <div class="col-12 elvas_development-collum px-1 px-md-3">
                                <div class="elvas_development--button text-center">
                                    <a href="<?php the_field('link_da_obra') ?>">Consulte o andamento dessa obra</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<section class="page-empreendimento">
    <div class="container-fluid elvas">
        <div class="row elvas_content">
            <div class="col-12 col-md-5 elvas_content-collum px-1 px-md-3 d-flex align-items-center">
                <div class="container">
                    <div class="row elvas_development justify-content-end">
                        <?php if (have_rows('informacoes_zonas')) : ?>
                            <div class="col-12 col-lg-10  elvas_development-collum px-1 px-md-3">
                                <div class="elvas_development--local">
                                    <div class="elvas_development--local-title">
                                        <h3><strong>Locais de interesse</strong></h3>
                                    </div>
                                    <div class="elvas_development--local-endereco d-flex">
                                        <div class="elvas_development--local-endereco-icon">
                                            <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/local2.png" alt="" width="100%">
                                        </div>
                                        <div class="elvas_development--local-endereco-text">
                                            <p class="m-0"><?php the_field('endereco') ?></p>
                                        </div>
                                    </div>
                                    <div class="elvas_development--local-info">
                                        <ul>
                                            <?php

                                            while (have_rows('informacoes_zonas')) : the_row();
                                            ?>
                                                <li>
                                                    <div class="elvas_development--local-info-list d-flex justify-content-between">
                                                        <div class="elvas_development--local-info-text">
                                                            <p><?php the_sub_field('informacoes_local') ?></p>
                                                        </div>
                                                        <div class="elvas_development--local-info-text">
                                                            <p><?php the_sub_field('valor_informacoes') ?></p>
                                                        </div>
                                                    </div>
                                                </li>
                                            <?php
                                            endwhile;

                                            ?>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        <?php endif;
                        if (!empty(get_field('imagem_mapa'))) : ?>
            <div class="col-12 col-md-7 elvas_content-collum px-0">
                <div class="elvas_development--local-map">
                    <img src="<?= the_field('imagem_mapa') ?>" alt="" style="width: 100%;">
                </div>
            </div>
        <?php endif; ?>
        </div>
    </div>
</section>
<section class="page-empreendimento">
    <div class="container-fluid elvas">
        <div class="row elvas_content pb-3">
            <div class="col-12 elvas_content-collum p-1 p-md-3  pb-md-5">
                <div class="container">
                    <div class="row elvas_development">
                        <div class="col-12 elvas_development-collum px-1 px-md-3">
                            <div class="elvas_development-title text-center">
                                <h2>Preencha o formulário <br> para saber mais</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row elvas_development mt-5">
                        <div class="col-12 elvas_development-collum px-1 px-md-3">
                            <div class="elvas_development-form">
                                <div class="elvas_development-box--item-form">
                                    <?= do_shortcode('[contact-form-7 id="113" title="Formulario Empreendimento"]') ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
<section class="page-empreendimento">
    <div class="container-fluid elvas">
        <div class="row elvas_content">
            <div class="col-12 elvas_content-collum px-1 px-md-3">
                <div class="container">
                    <div class="row elvas_development">
                        <?php if (get_field('empreendimentos_relacionados', false, false)) : ?>
                            <div class="col-12 elvas_development-collum">
                                <div class="elvas_development-title text-center">
                                    <h1>Não encontrou o que busca? <br>
                                        <strong>Veja mais opções</strong>
                                    </h1>
                                </div>
                            <?php endif; ?>
                            <div class="elvas_development-more">
                                <div class="elvas_development-list">
                                    <?php
                                    $ids = get_field('empreendimentos_relacionados', false, false);

                                    $empreendimento = new WP_Query(array(
                                        'post_type'          => 'empreendimento',
                                        'posts_per_page'    => 3,
                                        'post__in'        => $ids,
                                        'post_status'      => 'publish', //O status que ele encontra
                                        'suppress_filters' => true, //Caso haja filtros internos 
                                        'orderby' => array( //A forma que será ordenado os elementos. 
                                            'post_date' => 'DESC'
                                        )
                                    ));
                                    ?>

                                    <?php if ($empreendimento->have_posts() && !empty($ids)) :
                                        while ($empreendimento->have_posts()) : $empreendimento->the_post();  //O loop já pronto, com a variável "$posts" sendo um array que guarda a posição dos elementos.
                                    ?>
                                            <div class="elvas_development-list--item">
                                                <a href="<?php the_permalink() ?>">
                                                    <div class="elvas_development-card">
                                                        <div class="elvas_development-card--header">
                                                            <div class="elvas_development-card--header-image">
                                                                <img src="<?= get_field('imagem_fachada')['sizes']['large'] ?>" alt="<?php get_field('imagem_fachada')['alt'] ?>" width="100%">
                                                            </div>
                                                            <div class="elvas_development-card--header-link d-flex">
                                                                <h3><?php the_field('nome_do_empreendimento') ?></h3>
                                                                <span>&#10132;</span>
                                                            </div>
                                                        </div>
                                                        <div class="elvas_development-card--body d-flex">
                                                            <div class="elvas_development-card--body-info-area w-50">
                                                                <div class="elvas_development-card--body-info d-flex">
                                                                    <div class="elvas_development-card--body-info-icon">
                                                                        <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/double-bed.png" alt="" width="100%">
                                                                    </div>
                                                                    <div class="elvas_development-card--body-info-text">
                                                                        <p><?php the_field('quartos') ?></p>
                                                                    </div>
                                                                </div>
                                                                <div class="elvas_development-card--body-info d-flex">
                                                                    <div class="elvas_development-card--body-info-icon">
                                                                        <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/tag.png" alt="" width="100%">
                                                                    </div>
                                                                    <div class="elvas_development-card--body-info-text">
                                                                        <p><?php the_field('tag') ?></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="elvas_development-card--body-info-area w-50">
                                                                <div class="elvas_development-card--body-info d-flex">
                                                                    <div class="elvas_development-card--body-info-icon">
                                                                        <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/key-icon.png" alt="" width="100%">
                                                                    </div>
                                                                    <div class="elvas_development-card--body-info-text">
                                                                        <p>Entregue em <?php the_field('data_entrega') ?></p>
                                                                    </div>
                                                                </div>
                                                                <div class="elvas_development-card--body-info d-flex">
                                                                    <div class="elvas_development-card--body-info-icon">
                                                                        <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/local-mini.png" alt="" width="100%">
                                                                    </div>
                                                                    <div class="elvas_development-card--body-info-text">
                                                                        <p><?php the_field('endereco') ?></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elvas_development-card elvas_development-card-hover">
                                                        <div class="elvas_development-card--header">
                                                            <div class="elvas_development-card--header-image">
                                                                <img src="<?= get_field('imagem_fachada')['sizes']['large'] ?>" alt="<?php get_field('imagem_fachada')['alt'] ?>" width="100%">
                                                            </div>
                                                            <div class="elvas_development-card--header-link d-flex">
                                                                <h3><?php the_field('nome_do_empreendimento') ?></h3>
                                                                <span>&#10132;</span>
                                                            </div>
                                                            <div class="elvas_development-card--header-logo">
                                                                <img src="<?= get_field('logo_empreendimento')['sizes']['medium'] ?>" alt="<?php get_field('logo_empreendimento')['alt'] ?>" width="100%">
                                                            </div>
                                                        </div>
                                                        <div class="elvas_development-card--body">
                                                            <div class="elvas_development-card--body-info-area">
                                                                <div class="elvas_development-card--body-info d-flex">
                                                                    <div class="elvas_development-card--body-info-icon">
                                                                        <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/key-icon.png" alt="" width="100%">
                                                                    </div>
                                                                    <div class="elvas_development-card--body-info-text">
                                                                        <p>Entregue em <?php the_field('data_entrega') ?></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="elvas_development-card--body-info-link">
                                                                <span>Conheça +</span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </a>
                                            </div>
                                        <?php endwhile; ?>
                                    <?php endif; ?>
                                    <?php wp_reset_postdata(); ?>
                                </div>
                            </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>