<?php

/**
 * Template Name: Portfólio
 */

get_header();
?>
<section class="page-portfolio">
    <div class="container-fluid elvas">
        <div class="row elvas_content">
            <div class="col-12 elvas_content-collum px-1 px-md-3">
                <div class="container">
                    <div class="row elvas_development">
                        <div class="col-12 elvas_development-collum px-1 px-md-3">
                            <div class="elvas_development-title text-center">
                                <h1><strong>Estes sonhos ja foram realizados</strong></h1>
                            </div>
                        </div>
                    </div>
                    <div class="row elvas_development">
                        <div class="col-12 elvas_development-collum px-1 px-md-3">
                            <div class="elvas_development-list">
                                <?php
                                $empreendimento = new WP_Query( //Estrutura pronta para chamar os Posts Types
                                    array(
                                        'posts_per_page'   => -1, //Defina quantos posts irão aparecer por página
                                        'post_type'        => 'empreendimento', //Defina o tipo de post que está setando
                                        'post_status'      => 'publish', //O status que ele encontra
                                        'suppress_filters' => true, //Caso haja filtros internos 
                                        'meta_query'       => array( // Aqui voce define as condicionais para o elemento existir.
                                            'relation' => 'AND', // Parâmetro default
                                            'portfolio' => array( //É o campo(rotulo do custom field) que voce chama
                                                'key' => 'portfolio', //Repete o rotulo aqui
                                                'value' => 'Sim' //O valor para algo acontecer
                                            )
                                        ),
                                        'orderby' => array( //A forma que será ordenado os elementos. 
                                            'post_date' => 'DESC'
                                        )
                                    )
                                );
                                ?>

                                <?php if ($empreendimento->have_posts()) :
                                    while ($empreendimento->have_posts()) : $empreendimento->the_post();  //O loop já pronto, com a variável "$posts" sendo um array que guarda a posição dos elementos.
                                ?>
                                        <div class="elvas_development-list--item">
                                            <div class="elvas_development-box">
                                                <div class="elvas_development-box--item side-title">
                                                    <div class="elvas_development-box--item-content">
                                                        <?php the_field('caixa_texto') ?>
                                                    </div>
                                                </div>
                                                <div class="elvas_development-box--item side-empree">
                                                    <a href="<?= the_permalink() ?>">
                                                        <div class="elvas_development-box--item-header">
                                                            <div class="elvas_development-box--item-image">
                                                                <img src="<?= get_field('banner')['sizes']['large'] ?>" alt="<?php get_field('banner')['alt'] ?>" width="100%">
                                                            </div>
                                                            <div class="elvas_development-box--item-link">
                                                                <div class="elvas_development-box--item-title">
                                                                    <h2><?= get_the_title() ?></h2>
                                                                    <span> &#10132;</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-4 xs-100">
                                                                <div class="elvas_development-box--item-info-image">
                                                                <img src="<?= get_field('logo_empreendimento')['sizes']['large'] ?>" alt="<?php get_field('logo_empreendimento')['alt'] ?>" width="100%">
                                                                </div>
                                                            </div>
                                                            <div class="col-8 xs-100">
                                                                <div class="elvas_development-box--item-info--content">
                                                                    <div class="elvas_development-box--item-info-area">
                                                                        <div class="elvas_development-box--item-info d-flex">
                                                                            <div class="elvas_development-box--item-info-icon">
                                                                                <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/double-bed.png" alt="" width="100%">
                                                                            </div>
                                                                            <div class="elvas_development-box--item-info-text">
                                                                                <p><?php the_field('quartos') ?></p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="elvas_development-box--item-info d-flex">
                                                                            <div class="elvas_development-box--item-info-icon">
                                                                                <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/tag.png" alt="" width="100%">
                                                                            </div>
                                                                            <div class="elvas_development-box--item-info-text">
                                                                                <p><?php the_field('tag') ?></p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elvas_development-box--item-info-area">
                                                                        <div class="elvas_development-box--item-info d-flex">
                                                                            <div class="elvas_development-box--item-info-icon">
                                                                                <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/key-icon.png" alt="" width="100%">
                                                                            </div>
                                                                            <div class="elvas_development-box--item-info-text">
                                                                                <p>Entregue em <?php the_field('data_entrega') ?></p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="elvas_development-box--item-info d-flex">
                                                                            <div class="elvas_development-box--item-info-icon">
                                                                                <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/local-mini.png" alt="" width="100%">
                                                                            </div>
                                                                            <div class="elvas_development-box--item-info-text">
                                                                                <p><?php the_field('endereco') ?></p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elvas_development-box--item-info-area">
                                                                        <div class="elvas_development-box--item-info d-flex">
                                                                            <div class="elvas_development-box--item-info-icon">
                                                                                <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/double-bed.png" alt="" width="100%">
                                                                            </div>
                                                                            <div class="elvas_development-box--item-info-text">
                                                                                <p><?php the_field('metros_quadrados') ?></p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endwhile; ?>
                                <?php endif; ?>
                                <?php wp_reset_postdata(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php get_footer(); ?>