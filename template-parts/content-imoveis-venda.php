<?php

/**
 * Template Name: Imoveis à venda
 */

get_header();
?>
<section class="page-vendas">
    <div class="container-fluid elvas">
        <div class="row elvas_content">
            <div class="col-12 elvas_content-collum px-1 px-md-3">
                <div class="container">
                    <div class="row elvas_development">
                        <div class="col-12 elvas_development-collum px-1 px-md-3">
                            <div class="elvas_development-title">
                                <h1><strong>O imóvel dos seus sonhos está aqui</strong></h1>
                            </div>
                        </div>
                    </div>
                    <div class="row elvas_development">
                        <div class="col-12 col-md-3 elvas_development-collum px-1 px-md-3">
                            <div class="elvas_development-category">
                                <div class="dropdown show">
                                    <a data-toggle="collapse" href="#collapseCategory" role="button" aria-expanded="false" aria-controls="collapseCategory">
                                        Todos <svg style="width: 10px;margin-left: 10px;fill: #a8a8a8;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512">
                                            <!--! Font Awesome Pro 6.1.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. -->
                                            <path d="M192 384c-8.188 0-16.38-3.125-22.62-9.375l-160-160c-12.5-12.5-12.5-32.75 0-45.25s32.75-12.5 45.25 0L192 306.8l137.4-137.4c12.5-12.5 32.75-12.5 45.25 0s12.5 32.75 0 45.25l-160 160C208.4 380.9 200.2 384 192 384z" />
                                        </svg>
                                    </a>
                                    <div class="collapse show" id="collapseCategory">
                                        <a class="dropdown-item" href="./?status=Lançamento">Lançamentos</a>
                                        <a class="dropdown-item" href="./?status=Em Construção">Construção</a>
                                        <a class="dropdown-item" href="./?status=Pronto para Morar">Prontos</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-9 elvas_development-collum px-1 px-md-3">
                            <div class="elvas_development-list">
                                <?php
                                $data = array(
                                    'posts_per_page'   => -1, //Defina quantos posts irão aparecer por página
                                    'post_type'        => 'empreendimento', //Defina o tipo de post que está setando
                                    'post_status'      => 'publish', //O status que ele encontra
                                    'suppress_filters' => true, //Caso haja filtros internos 
                                    'orderby' => array( //A forma que será ordenado os elementos. 
                                        'post_date' => 'DESC'
                                    )
                                );

                      
                                if (isset($_GET['status'])) {
                                    $data['meta_query'] = array( // Aqui voce define as condicionais para o elemento existir.
                                        'relation' => 'AND', // Parâmetro default
                                        'status_obra' => array( //É o campo(rotulo do custom field) que voce chama
                                            'key' => 'status_obra', //Repete o rotulo aqui
                                            'value' => $_GET['status'] //O valor para algo acontecer
                                        )
                                    );
                                }

                                $empreendimento = new WP_Query($data);
                                ?>

                                <?php if ($empreendimento->have_posts()) :
                                    while ($empreendimento->have_posts()) : $empreendimento->the_post();  //O loop já pronto, com a variável "$posts" sendo um array que guarda a posição dos elementos.
                                ?>
                                        <div class="elvas_development-list--item">
                                            <a href="<?php the_permalink() ?>">
                                                <div class="elvas_development-card">
                                                    <div class="elvas_development-card--header">
                                                        <div class="elvas_development-card--header-image">
                                                            <img src="<?= get_field('imagem_fachada')['sizes']['large'] ?>" alt="<?php get_field('imagem_fachada')['alt'] ?>" width="100%">
                                                        </div>
                                                        <div class="elvas_development-card--header-link d-flex">
                                                            <h3><?php the_field('nome_do_empreendimento') ?></h3>
                                                            <span>&#10132;</span>
                                                        </div>
                                                    </div>
                                                    <div class="elvas_development-card--body d-flex">
                                                        <div class="elvas_development-card--body-info-area w-50">
                                                            <div class="elvas_development-card--body-info d-flex">
                                                                <div class="elvas_development-card--body-info-icon">
                                                                    <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/double-bed.png" alt="" width="100%">
                                                                </div>
                                                                <div class="elvas_development-card--body-info-text">
                                                                    <p><?php the_field('quartos') ?></p>
                                                                </div>
                                                            </div>
                                                            <div class="elvas_development-card--body-info d-flex">
                                                                <div class="elvas_development-card--body-info-icon">
                                                                    <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/tag.png" alt="" width="100%">
                                                                </div>
                                                                <div class="elvas_development-card--body-info-text">
                                                                    <p><?php the_field('tag') ?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elvas_development-card--body-info-area w-50">
                                                            <div class="elvas_development-card--body-info d-flex">
                                                                <div class="elvas_development-card--body-info-icon">
                                                                    <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/key-icon.png" alt="" width="100%">
                                                                </div>
                                                                <div class="elvas_development-card--body-info-text">
                                                                    <p>Entregue em <?php the_field('data_entrega') ?></p>
                                                                </div>
                                                            </div>
                                                            <div class="elvas_development-card--body-info d-flex">
                                                                <div class="elvas_development-card--body-info-icon">
                                                                    <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/local-mini.png" alt="" width="100%">
                                                                </div>
                                                                <div class="elvas_development-card--body-info-text">
                                                                    <p><?php the_field('endereco') ?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elvas_development-card elvas_development-card-hover">
                                                    <div class="elvas_development-card--header">
                                                        <div class="elvas_development-card--header-image">
                                                            <img src="<?= get_field('imagem_fachada')['sizes']['large'] ?>" alt="<?php get_field('imagem_fachada')['alt'] ?>" width="100%">
                                                        </div>
                                                        <div class="elvas_development-card--header-link d-flex">
                                                            <h3><?php the_field('nome_do_empreendimento') ?></h3>
                                                            <span>&#10132;</span>
                                                        </div>
                                                        <div class="elvas_development-card--header-logo">
                                                            <img src="<?= get_field('logo_empreendimento')['sizes']['medium'] ?>" alt="<?php get_field('logo_empreendimento')['alt'] ?>" width="100%">
                                                        </div>
                                                    </div>
                                                    <div class="elvas_development-card--body">
                                                        <div class="elvas_development-card--body-info-area">
                                                            <div class="elvas_development-card--body-info d-flex">
                                                                <div class="elvas_development-card--body-info-icon">
                                                                    <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/key-icon.png" alt="" width="100%">
                                                                </div>
                                                                <div class="elvas_development-card--body-info-text">
                                                                    <p>Entregue em <?php the_field('data_entrega') ?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elvas_development-card--body-info-link">
                                                            <span>Conheça +</span>
                                                        </div>
                                                    </div>

                                                </div>
                                            </a>
                                        </div>
                                    <?php endwhile; ?>
                                <?php else : ?>
                                    <p class="text-center">Nenhum post encontrado</p>
                                <?php endif; ?>
                                <?php wp_reset_postdata(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php get_footer(); ?>