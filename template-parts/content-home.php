<?php

/**
 * Template Name: Home
 */

get_header();
?>

<main class="page-home">
    <div class="container-fluid elvas">
        <div class="row elvas_content">
            <div class="col-12 elvas_content-collum px-0 ">
                <div class="elvas_development--main-carousel">
                    <div class="swiper-container js-swiper-main">
                        <!-- Wrapper adicional necessário -->
                        <div class="swiper-wrapper">
                            <!-- Slides -->
                            <?php
                            $empreendimento = new WP_Query( //Estrutura pronta para chamar os empreendimento Types
                                array(
                                    'empreendimento_per_page'   => -1, //Defina quantos empreendimento irão aparecer por página
                                    'post_type'        => 'empreendimento', //Defina o tipo de post que está setando
                                    'post_status'      => 'publish', //O status que ele encontra
                                    'suppress_filters' => true, //Caso haja filtros internos 
                                    'orderby' => array( //A forma que será ordenado os elementos. 
                                        'post_date' => 'ASC'
                                    )
                                )
                            );
                            ?>

                            <?php if ($empreendimento->have_posts()) :
                                while ($empreendimento->have_posts()) : $empreendimento->the_post();  //O loop já pronto, com a variável "$posts" sendo um array que guarda a posição dos elementos.
                            ?>
                                    <div class="swiper-slide">
                                        <div class="background-filter"></div>
                                        <div class="elvas_content-block">
                                            <div class="elvas_content-block--info">
                                                <div class="elvas_content-background">
                                                    <div class="elvas_content-background-image">
                                                        <img src="<?= get_field('imagem_preview')['sizes']['large'] ?>" alt="<?= get_field('imagem_preview')['alt'] ?>">
                                                    </div>
                                                </div>
                                                <div class="elvas_content-block--list">
                                                    <div class="elvas_content-block-title">
                                                        <h2><strong><?= mb_strtoupper(get_field('status_obra')) ?></strong></h2>
                                                    </div>
                                                    <div class="elvas_content-block-subtitle">
                                                        <h3><strong><?= get_field('titulo_apresentacao') ?></strong></h3>
                                                    </div>
                                                    <div class="elvas_content-block-text">
                                                        <p>
                                                            <?= get_field('subtitulo_apresentacao') ?>
                                                        </p>
                                                    </div>
                                                    <div class="elvas_content-block-button">
                                                        <a href="<?= the_permalink(); ?>">Saiba mais &#10132;</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>
                            <?php wp_reset_postdata(); ?>
                        </div>
                    </div>
                </div>
                <div class="elvas_development--thumb-carousel js-content-bar">
                    <div class="elvas_development--thumb-carousel-logo-btn js-btn-bar">
                        <button>&#8644;</button>
                    </div>
                    <div class="swiper-container js-swiper-main-thumbnail">
                        <!-- Wrapper adicional necessário -->
                        <div class="swiper-wrapper">
                            <!-- Slides -->
                            <?php if ($empreendimento->have_posts()) :
                                while ($empreendimento->have_posts()) : $empreendimento->the_post();  //O loop já pronto, com a variável "$posts" sendo um array que guarda a posição dos elementos.
                            ?>
                                    <div class="swiper-slide">
                                        <div class="elvas_development--thumb-carousel-logo">
                                            <img src="<?= get_field('logo_empreendimento')['sizes']['large'] ?>" alt="<?= get_field('logo_empreendimento')['alt'] ?>">
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>
                            <?php wp_reset_postdata(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<section class="page-home">
    <div class="container-fluid elvas">
        <div class="row elvas_content">
            <div class="col-12 elvas_content-collum px-1 px-md-3">
                <div class="container">
                    <div class="row elvas_development">
                        <div class="col-12 elvas_development-collum px-1 px-md-3">
                            <div class="elvas_development-title">
                                <h2><strong>O imóvel dos seus sonhos está aqui</strong></h2>
                            </div>
                        </div>
                    </div>
                    <div class="row elvas_development">
                        <div class="col-12 elvas_development-collum px-1 px-md-3">
                            <div class="elvas_development-list">
                                <div class="elvas_development-list--item">
                                    <div class="elvas_development-info">
                                        <div class="elvas_development-title">
                                            <h2>Nossos <br><strong>Empreendimentos</strong></h2>
                                        </div>
                                        <div class="elvas_development-text">
                                            <p>Conheça nossos empreendimentos e curta o diferencial de cada um deles. Certamente existe um que combina com você.</p>
                                        </div>
                                        <div class="elvas_development-link">
                                            <a href="./imoveis-a-venda">Ver tudo &#10132;</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Slides -->
                                <?php if ($empreendimento->have_posts()) :
                                    while ($empreendimento->have_posts()) : $empreendimento->the_post();  //O loop já pronto, com a variável "$posts" sendo um array que guarda a posição dos elementos.
                                ?>
                                        <div class="elvas_development-list--item">
                                            <div class="elvas_development-illustration">
                                                <div class="elvas_development-box--item-image">
                                                    <img src="<?= get_field('imagem_fachada')['sizes']['large'] ?>" alt="<?= get_field('imagem_fachada')['alt'] ?>">
                                                </div>
                                                <a href="<?= the_permalink() ?>">
                                                    <div class="elvas_development-box-name">
                                                        <div class="elvas_development-box--item-subtitle-info">
                                                            <p><?= get_field('status_obra') ?></p>
                                                        </div>
                                                        <div class="elvas_development-box--item-title">
                                                            <h2><?= get_field('nome_do_empreendimento') ?></h2>
                                                            <div class="elvas_development-box--item-link">
                                                                <span>&#10132;</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="elvas_development-box--item-info">
                                                <p><img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/regua.png" alt="logotipo regua"> <?php the_field('metros_quadrados') ?></p>
                                                <p><img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/local-mini.png" alt="logotipo local"> <?php the_field('endereco') ?> </p>
                                            </div>
                                        </div>
                                    <?php endwhile; ?>
                                <?php endif; ?>
                                <?php wp_reset_postdata(); ?>

                                <div class="elvas_development-list--item">
                                    <div class="elvas_development-box">
                                        <div class="elvas_development-box--item">
                                            <div class="elvas_development-box--item-title">
                                                <h4>Contruindo <br><strong>Qualidade de Vida</strong></h4>
                                            </div>
                                            <div class="elvas_development-box--item-text">
                                                <p>Grandes desafios de engenharia e arquitetura requerem uma empresa moderna, cuja experiência no mercado imobiliário se mede pela qualidade dos empreendimentos residenciais, comerciais e hoteleiros concluídos.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elvas_development-icons">
                                <div class="elvas_development-icons-background">
                                    <div class="row elvas_development">
                                        <div class="col-12">
                                            <div class="elvas_development-icons-box">
                                                <div class="elvas_development-icons-box--item">
                                                    <div class="elvas_development-icons-box--item-mark">
                                                        <span>"</span>
                                                    </div>
                                                    <div class="elvas_development-icons-box--item-title">
                                                        <h2 data-counter="55">00</h2>
                                                    </div>
                                                    <div class="elvas_development-icons-box--item-list">
                                                        <div class="elvas_development-icons-box--item-list-text">
                                                            <p>Obras <br> Realizadas.</p>
                                                        </div>
                                                        <div class="elvas_development-icons-box--item-list-image">
                                                            <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/guindaste-branco.png" alt="Icon guindaste">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elvas_development-icons-box--item">
                                                    <div class="elvas_development-icons-box--item-mark">
                                                        <span>"</span>
                                                    </div>
                                                    <div class="elvas_development-icons-box--item-title">
                                                        <h2 data-counter="2990">00</h2>
                                                    </div>
                                                    <div class="elvas_development-icons-box--item-list">
                                                        <div class="elvas_development-icons-box--item-list-text">
                                                            <p>Unidades <br> Entregues.</p>
                                                        </div>
                                                        <div class="elvas_development-icons-box--item-list-image">
                                                            <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/predios-branco.png" alt="Icon predio">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elvas_development-icons-box--item">
                                                    <div class="elvas_development-icons-box--item-mark">
                                                        <span>"</span>
                                                    </div>
                                                    <div class="elvas_development-icons-box--item-title">
                                                        <h2><span data-counter="20">00</span> Anos</h2>
                                                    </div>
                                                    <div class="elvas_development-icons-box--item-list">
                                                        <div class="elvas_development-icons-box--item-list-text">
                                                            <p>Realizando <br> Sonhos.</p>
                                                        </div>
                                                        <div class="elvas_development-icons-box--item-list-image">
                                                            <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/pessoas-branco-2.png" alt="Icon pessoas">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="page-home mt-5">
    <div class="container-fluid elvas">
        <div class="row elvas_content">
            <div class="col-12 elvas_content-collum px-1 px-md-3">
                <div class="container">
                    <div class="row elvas_development">
                        <div class="col-12 col-md-6 elvas_development-collum px-1 px-md-3">
                            <div class="elvas_development-info">
                                <div class="elvas_development-info-content">
                                    <div class="elvas_development-info-title">
                                        <h5><strong>Portal do Cliente</strong></h5>
                                    </div>
                                    <div class="elvas_development-info-text">
                                        <p>
                                            Acasse os atalhos para acompanhar a sua obra, acessar a segunda vida de 2 boletos e muito mais, basta clicar nos atalhos ao lado.
                                        </p>
                                    </div>
                                    <div class="elvas_development-info-button">
                                        <a href="#">Logar no portal do cliente &#10132;</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 elvas_development-collum px-1 px-md-3">
                            <div class="elvas_content-box">
                                <div class="elvas_development-box--item">
                                    <a href="#">
                                        <div class="elvas_development-box--item-image">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512">
                                                <!--! Font Awesome Pro 6.0.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. -->
                                                <path d="M365.3 93.38l-74.63-74.64C278.6 6.742 262.3 0 245.4 0L64-.0001c-35.35 0-64 28.65-64 64l.0065 384c0 35.34 28.65 64 64 64H320c35.2 0 64-28.8 64-64V138.6C384 121.7 377.3 105.4 365.3 93.38zM336 448c0 8.836-7.164 16-16 16H64.02c-8.838 0-16-7.164-16-16L48 64.13c0-8.836 7.164-16 16-16h160L224 128c0 17.67 14.33 32 32 32h79.1V448zM96 280C96 293.3 106.8 304 120 304h144C277.3 304 288 293.3 288 280S277.3 256 264 256h-144C106.8 256 96 266.8 96 280zM264 352h-144C106.8 352 96 362.8 96 376s10.75 24 24 24h144c13.25 0 24-10.75 24-24S277.3 352 264 352z" />
                                            </svg>
                                        </div>
                                        <div class="elvas_development-box--item-text">
                                            <p>2 Via <br> do boleto</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="elvas_development-box--item">
                                    <a href="#">
                                        <div class="elvas_development-box--item-image">
                                            <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/headset.png" alt="Icon assistencia tecnica" width="100%">
                                        </div>
                                        <div class="elvas_development-box--item-text">
                                            <p>Assistência <br> Técnica</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="elvas_development-box--item">
                                    <a href="#">
                                        <div class="elvas_development-box--item-image">
                                            <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/download.png" alt="Icon download" width="100%">
                                        </div>
                                        <div class="elvas_development-box--item-text">
                                            <p>Download <br> aqui</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="elvas_development-box--item">
                                    <a href="#">
                                        <div class="elvas_development-box--item-image">
                                            <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/assistencia-ferramentas.png" alt="Icon ferramentas" width="100%">
                                        </div>
                                        <div class="elvas_development-box--item-text">
                                            <p>Acompanhe <br> a obra</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="page-home">
    <div class="container-fluid elvas">
        <div class="row elvas_content">
            <div class="col-12 elvas_content-collum px-0">
                <div class="col-12 elvas_development-collum px-0">
                    <a href="https://goo.gl/maps/3EaKzdm4xrTXYJ3f6" target="_blank">
                        <img src="<?= get_template_directory_uri() ?>/assets/src/img/google-maps.png" style="width: 100%; height: 100%" alt="Google Maps">
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>


<?php get_footer(); ?>