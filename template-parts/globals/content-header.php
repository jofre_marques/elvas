<header>
    <div class="container-fluid elvas">
        <div class="row elvas_content">
            <div class="col-12 elvas_content-column px-0">
                <div class="container-lg">
                    <div class="row elvas_development">
                        <div class="col-12 elvas_development-column px-0">
                            <div class="elvas_development-nav">
                                <div class="elvas_development-nav--logo">
                                    <?php the_custom_logo() ?>
                                </div>
                                <div class="elvas_development-nav-search">
                                    <div class="elvas_development-nav-search-bar">
                                        <?php get_search_form() ?>
                                    </div>
                                </div>
                                <div class="elvas_development-nav-navbar">
                                    <nav >
                                        <button class="menu-toggle"> 
                                        
                                        <div class="elvas_development-nav-navbar-img">
                                        <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/menu.png" alt="Icon menu">
                                        </div></button>
                                        <?php
                                        wp_nav_menu(
                                            array(
                                                'theme_location' => 'menu-1',
                                                'menu_id'        => 'primary-menu',
                                            )
                                        );
                                        ?>
                                    </nav><!-- #site-navigation -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>