<footer>
    <div class="container-fluid elvas my-4">
        <div class="row elvas_content">
            <div class="col-12 elvas_content-column">
                <div class="container">
                    <div class="row elvas_development">
                        <div class="col-12 col-md-2 elvas_development-column">
                            <div class="elvas_development-nav">
                                <div class="elvas_development-nav--logo">
                                    <a href="<?= site_url() ?>">
                                        <img src="<?= get_template_directory_uri() ?>/assets/src/img/logos/logo-preta.png" alt="Logo da Elvas">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-2 col-md-3 mt-3 elvas_development-column">
                            <div class="elvas_development-contato">
                                <div class="elvas_development-contato-title">
                                    <h4>CONTATO</h4>
                                </div>
                                <div class="elvas_development-contato-list">
                                    <ul>
                                        <li><a href="tel: (21) 2010-1900">(21) 2010-1900</a></li>
                                        <li><a href="mailto:Contato@mail.com">Contato@mail.com</a></li>
                                        <li><a href="https://goo.gl/maps/3EaKzdm4xrTXYJ3f6">Rua Miguel de Frias, 77701, <br> Icaraí - Niterói RJ 24220-001</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-2 px-0 pl-2 mt-3 elvas_development-column">
                            <div class="elvas_development-redes">
                                <div class="elvas_development-redes-title">
                                    <h4>REDES SOCIAIS</h4>
                                </div>
                                <div class="elvas_development-redes-list">
                                    <ul>
                                        <li>
                                            <a href="">
                                                <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/facebook.png" alt="Icon Facebook"> Facebook
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-6 col-md-5 mt-3">
                            <div class="elvas_development-navbar">
                                <div class="elvas_development-navbar-title">
                                    <h4>MENU</h4>
                                </div>
                                <nav id="site-navigation" class="main-navigation">

                                    <?php
                                    wp_nav_menu(
                                        array(
                                            'theme_location' => 'menu-1',
                                        )
                                    );
                                    ?>
                                </nav><!-- #site-navigation -->
                            </div>
                        </div>
                    </div>
                    <div class="row elvas_development">
                        <div class="col-12 elvas_development-collum">
                            <div class="internit-mark">
                                <a href="https://internit.com.br" target="_blank">
                                    <img src="<?= get_template_directory_uri() ?>/assets/src/img/logos/logo-internit.png" alt="" width="100%">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>