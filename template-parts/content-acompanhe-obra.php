<?php

/**
 * Template Name: Acompanhe a Obra
 */

get_header();
?>
<section class="page-acompanhe-obra">
    <div class="container-fluid elvas">
        <div class="row elvas_content">
            <div class="col-12 elvas_content-collum px-1 px-md-3">
                <div class="container">
                    <div class="row elvas_development">
                        <div class="col-12 elvas_development-collum px-1 px-md-3">
                            <div class="elvas_development-title">
                                <h1>Acompanhe a Obra</h1>
                            </div>
                        </div>
                    </div>
                    <div class="row elvas_development">
                        <div class="col-12 elvas_development-collum px-1 px-md-3">
                            <?php
                            $empreendimento = new WP_Query( //Estrutura pronta para chamar os Posts Types
                                array(
                                    'posts_per_page'   => -1, //Defina quantos posts irão aparecer por página
                                    'post_type'        => 'empreendimento', //Defina o tipo de post que está setando
                                    'post_status'      => 'publish', //O status que ele encontra
                                    'suppress_filters' => true, //Caso haja filtros internos 
                                    'meta_query'       => array( // Aqui voce define as condicionais para o elemento existir.
                                        'relation' => 'AND', // Parâmetro default
                                        'status_obra' => array( //É o campo(rotulo do custom field) que voce chama
                                            'key' => 'status_obra', //Repete o rotulo aqui
                                            'value' => 'Em Construção' //O valor para algo acontecer
                                        )
                                    ),
                                    'orderby' => array( //A forma que será ordenado os elementos. 
                                        'post_date' => 'DESC'
                                    )
                                )
                            );
                            ?>

                            <?php if ($empreendimento->have_posts()) :
                                while ($empreendimento->have_posts()) : $empreendimento->the_post();  //O loop já pronto, com a variável "$posts" sendo um array que guarda a posição dos elementos.
                            ?>
                                    <div class="elvas_development-list">
                                        <div class="elvas_development-list--item">
                                            <div class="elvas_development-image">
                                                <img src="<?= get_field('banner')['sizes']['large'] ?>" alt="<?php get_field('banner')['alt'] ?>" width="100%">
                                            </div>
                                        </div>
                                        <div class="elvas_development-list--item">
                                            <div class="elvas_development-box d-flex">
                                                <div class="elvas_development-box--item">
                                                    <div class="elvas_development-box--item-image">
                                                        <div class="elvas_development-box--item-image-logo">
                                                            <img src="<?= get_field('logo_empreendimento')['sizes']['medium'] ?>" alt="<?php get_field('logo_empreendimento')['alt'] ?>" width="100%">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elvas_development-box--item">
                                                    <div class="elvas_development-box--item-info">
                                                        <div class="elvas_development-box--item-info-account">
                                                            <div class="pie animate m-0" style="--p:<?= the_field('porcentagem_da_obra') ?>;">
                                                                <p><?= the_field('porcentagem_da_obra') ?>%</p>
                                                                <span class="bg-account"></span>
                                                            </div>
                                                        </div>
                                                        <div class="elvas_development-box--item-info-text mt-2">
                                                            <p><strong>Acompanhe esta obra</strong></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="elvas_development-link text-center">
                                                <div class="elvas_development-link-text">
                                                    <a href="<?= the_permalink()?>"><strong>Conheça +</strong></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            <?php else : ?>
                                <p class="text-center">Nenhuma obra encontrada</p>
                            <?php endif; ?>
                            <?php wp_reset_postdata(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php get_footer(); ?>