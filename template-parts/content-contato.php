<?php

/**
 * Template Name: Contato
 */

get_header();
?>
<section class="page-contato">
    <div class="container-fluid elvas">
        <div class="row elvas_content">
            <div class="col-12 elvas_content-collum px-1 px-md-3">
                <div class="container">
                    <div class="row elvas_development">
                        <div class="col-12 elvas_development-collum px-1 px-md-3">
                            <div class="elvas_development-titulo">
                                <div class="elvas_development-title">
                                    <h2>Entre em contato conosco</h2>
                                </div>
                                <div class="elvas_development-subtitle">
                                    <p> Se você tiver alguma duvida, pergunta ou sugestão, entre em contato conosco</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row elvas_development">
                        <div class="col-12 elvas_development-collum px-1 px-md-3">
                            <div class="elvas_development-list">
                                <div class="elvas_development-list--item">
                                    <div class="elvas_development-box">
                                        <div class="elvas_development-box--item">
                                            <div class="elvas_development-box--item-text-obg">
                                                <p>*Campos de preenchimento obrigatório </p>
                                            </div>
                                            <div class="elvas_development-box--item-form">
                                                <?= do_shortcode('[contact-form-7 id="87" title="pagina contato"]') ?>
                                                <!-- <form action="">
                                                    <div class="elvas_development-box-info d-flex">
                                                        <div class="elvas_development-box-w50">
                                                            <div class="elvas_development-box--item-select">
                                                                <select>
                                                                    <option>Departamento*</option>
                                                                    <option value="1">Departamento1</option>
                                                                    <option value="2">Departamento2</option>
                                                                    <option value="3">Departamento3</option>
                                                                    <option value="4">Departamento4</option>
                                                                    <option value="5">Departamento5</option>
                                                                    <option value="6">Departamento6</option>
                                                                </select>
                                                            </div>
                                                            <div class="elvas_development-box--item-email">
                                                                <input type="email" placeholder="E-mail*">
                                                            </div>
                                                            <div class="elvas_development-box--item-country d-flex">
                                                                <div class="elvas_development-box--item-country1">
                                                                    <div class="elvas_development-box--item-select">
                                                                        <select>
                                                                            <option>Pais*</option>
                                                                            <option value="1">Brasil</option>
                                                                            <option value="2">Brasil</option>
                                                                            <option value="3">Brasil</option>
                                                                            <option value="4">Brasil</option>
                                                                            <option value="5">Brasil</option>
                                                                            <option value="6">Brasil</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="elvas_development-box--item-number">
                                                                    <input type="text" placeholder="(DDD)Celular*">
                                                                </div>
                                                            </div>
                                                            <div class="elvas_development-box--item-country d-flex">
                                                                <div class="elvas_development-box--item-country2">
                                                                    <div class="elvas_development-box--item-select">
                                                                        <select>
                                                                            <option>Pais</option>
                                                                            <option value="1">Brasil</option>
                                                                            <option value="2">Brasil</option>
                                                                            <option value="3">Brasil</option>
                                                                            <option value="4">Brasil</option>
                                                                            <option value="5">Brasil</option>
                                                                            <option value="6">Brasil</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="elvas_development-box--item-number2">
                                                                    <input type="text" placeholder="(DDD)Telefone Fixo">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elvas_development-box-w50">
                                                            <div class="elvas_development-box--item-name">
                                                                <input type="text" placeholder="Nome*">
                                                            </div>
                                                            <div class="elvas_development-box--item-select2">
                                                                <select>
                                                                    <option>Como ficou sabendo da empresa?*</option>
                                                                    <option value="1">Departamento1</option>
                                                                    <option value="2">Departamento2</option>
                                                                    <option value="3">Departamento3</option>
                                                                    <option value="4">Departamento4</option>
                                                                    <option value="5">Departamento5</option>
                                                                    <option value="6">Departamento6</option>
                                                                </select>
                                                            </div>

                                                            <div class="elvas_development-box--item ">
                                                                <div class="elvas_development-box--item-text-area">
                                                                    <textarea name="" id="" cols="50" rows="3" placeholder="Mensagem*"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elvas_development-box">
                                                        <div class="elvas_development-box-checkbox  d-flex">
                                                            <label>
                                                                <input type="checkbox">
                                                                <span>
                                                                    Eu estou ciente de que um representante da Elvas Empreendimentos ira entrar em contato comigo. também confirmo que li e concordo com os termos e condições deste site.
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="elvas_development-box-enviar text-center">
                                                        <input id="submit_button" type="submit" value="Enviar" />
                                                    </div>
                                                </form> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row elvas_development">
                        <div class="col-12 col-md-6 elvas_development-collum">
                            <div class="elvas_development-collum">
                                <div class="elvas_development-canais">
                                    <div class="elvas_development-title">
                                        <h4>Canais de Comunicação</h4>
                                    </div>
                                    <div class="elvas_development-text">
                                        <p>Com nossos diversos canais de comuinicação, nos da Elvas Empreendimentos e você podemos nos comunicar por telefone, internet ou pelo nosso escritório.Estamos prontos para lhe atender.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 elvas_development-collum px-1 px-md-3">
                            <div class="elvas_development-collum">
                                <div class="elvas_development-contato d-flex">
                                    <div class="elvas_development-contato-tell">
                                        <a href="#">
                                            <div class="elvas_development-contato-tell-icon">
                                                <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/tell.png" alt="" style="width: 100%;">
                                            </div>
                                            <p>(21)2010-1000</p>
                                            <h4>Escritório</h4>
                                        </a>
                                    </div>
                                    <div class="elvas_development-contato-cell">
                                        <a href="#">
                                            <div class="elvas_development-contato-cell-icon">
                                                <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/cell.png" alt="" style="width: 100%;">
                                            </div>
                                            <p>(21)90000-0000</p>
                                            <h4>Stand de <br> Vendas</h4>
                                        </a>
                                    </div>
                                </div>
                                <div class="elvas_development-address">
                                    <div class="elvas_development-address-local" style="">
                                        <a href="#">
                                            <div class="elvas_development-address-icon">
                                                <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/local white.png" alt="" style="width: 100%;">
                                            </div>
                                            <p>R. Miguel Frias,nº 77/ sala 701 <br> Icaraí - Niterói - RJ</p>
                                            <h2>Endereço</h2>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="page-contato">
    <div class="container-fluid elvas">
        <div class="row elvas_development">
            <div class="col-12 elvas_development-collum px-0">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3675.3000996854516!2d-43.11743498452907!3d-22.90229934346987!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9983e73fb88101%3A0x5fba4e275da77421!2sR.%20Miguel%20de%20Frias%2C%2077701%20-%20Icara%C3%AD%2C%20Niter%C3%B3i%20-%20RJ%2C%2024220-001!5e0!3m2!1spt-BR!2sbr!4v1646860908718!5m2!1spt-BR!2sbr" width="1000" height="400" style="border:0; width:100%" allowfullscreen="" loading="lazy"></iframe>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>