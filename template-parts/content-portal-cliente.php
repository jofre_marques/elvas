<?php

/**
 * Template Name: Portal do Cliente
 */

get_header();
?>
<main class="login">
    <div class="login_background"></div>
    <div class="container-fluid elvas">
        <div class="row elvas_content">
            <div class="col-12 elvas_content-column">
                <div class="container">
                    <div class="row elvas_development">
                        <div class="col-12 elvas_development-column">
                            <div class="elvas_development-form">
                                <form name="logar" id="logar" method="post" action="#" enctype="multipart/form-data">

                                    <div class="form-title">
                                        <h1>Portal do Cliente</h1>
                                    </div>
                                    <div class="form-respost">
                                        <span id="resposta_form_logar" class="resposta_form"></span>
                                    </div>

                                    <div class="row my-2">
                                        <div class="col-12">
                                            <div class="form-group-login">
                                                <input type="text" name="login" placeholder="Nome de usuário">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row my-2">
                                        <div class="col-12">
                                            <div class="form-group-password" data-password>
                                                <input type="password" name="senha" placeholder="Senha">
                                                <span class="js-eye" style="width: 7%;"> <img src="<?= get_template_directory_uri() ?>/assets/src/img/icon/eyes-close.png" alt="" width="100%"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-btn">
                                        <button type="submit" value="OK">Entrar</button>
                                    </div>
                                </form>
                                <div class="elvas_development-form-forget">
                                    <a href="#">Esqueceu a senha?</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php
get_footer();
?>

<script>
    jQuery("#logar").submit(function(event) {
        event.preventDefault();

        var form = jQuery(this);

        var posting = jQuery.ajax({
            crossDomain: true,
            type:"POST",
            url: "#",
            data: form.serialize(),
        });

        //var posting = jQuery.post("https://www.sistemaengenharia.com.br/relatorio/login", form.serialize());

        posting.done(function(data) {
            var obj = JSON.parse(data);
            if(obj.sucesso)
            {
                window.location="#";
            }else{
                alert("Dados inválidos");
            }
        });
    });
</script>