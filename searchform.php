<form role="search" method="get" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <label>
        <span class="screen-reader-text"><?php _x( 'Search for:', 'label' ); ?></span>
        <input type="search" class="search-field" value="<?php echo get_search_query(); ?>" placeholder="Digite a cidade ou tipo de imóvel" name="s" />
        <input type="submit" id="searchsubmit"
            value="<?php echo esc_attr_x( 'Search', 'submit button' ); ?>" />
    </label>
</form>