let Carousel = function () {
    let $loc = new Array();

    this.swiper = function ($coord, $obj) {
        if (document.querySelector($coord)) {
            document.addEventListener("DOMContentLoaded", function () {
                var swiper = new Swiper($coord, $obj);
                return swiper;
            });
        }
    }

    this.bootstrap = function ($obj, $coord = ".boot-carousel") {
        if (document.querySelector($coord)) {
            document.addEventListener('DOMContentLoaded', () => {
                let carousel = document.querySelector($coord)
                let bootCarousel = new bootstrap.Carousel(carousel, $obj)
            })
        }
    }
}

let cmd = new Carousel();


// cmd.swiper({
//     // Optional parameters
//     direction: 'horizontal',
//     loop: true,

//     // If we need pagination
//     pagination: {
//         el: '.swiper-pagination',
//     },

//     // Navigation arrows
//     navigation: {
//         nextEl: '.swiper-button-next',
//         prevEl: '.swiper-button-prev',
//     },

//     // And if we need scrollbar
//     scrollbar: {
//         el: '.swiper-scrollbar',
//     },
// })




const thumb = new Swiper('.js-swiper-main-thumbnail', {
    spaceBetween: 10,
    slidesPerView: 3,
    loop: true,
    autoplay: {
        delay: 2500,
    },
    watchSlidesProgress: true,
    navigation: {
        nextEl: ".swiper-thumbnail-button-next",
        prevEl: ".swiper-thumbnail-button-prev",
    }

})

const bannerMain = new Swiper('.js-swiper-main', {
    spaceBetween: 10,
    effect: "fade",
    loop: true,
    autoplay: {
        delay: 2500,
    },
    thumbs: {
        swiper: thumb,
    },
})


/**
 * EMPREENDIMENTO GALERIA
 */

var swpGaleria = new Swiper(".js-thumbnail-galeria-empreendimento", {
    direction: 'vertical',
    spaceBetween: 15,
    slidesPerView: 3,
    loop: false,
    watchSlidesProgress: true,
});
var swpGaleriaThumb = new Swiper(".js-galeria-empreendimento", {
    loop: false,
    thumbs: {
        swiper: swpGaleria,
    },
});

/**
 * EMPREENDIMENTO DECORADO
 */

var swpDecorado = new Swiper(".js-thumbnail-decorado-empreendimento", {
    direction: 'vertical',
    slidesPerView: 3,
   
    watchSlidesProgress: true,
});
var swpDecoradoThumb = new Swiper(".js-decorado-empreendimento", {
    loop: true,
    spaceBetween: 10,
    navigation: {
        nextEl: ".swiper-thumbnail-decorado-button-next",
        prevEl: ".swiper-thumbnail-decorado-button-prev",
    },
    thumbs: {
        swiper: swpDecorado,
    },
});


var swpPerspectiva = new Swiper(".js-thumbnail-perspectiva-empreendimento", {
    direction: 'vertical',
    slidesPerView: 3,
    watchSlidesProgress: true,
});
var swpPerspectivaThumb = new Swiper(".js-perspectiva-empreendimento", {
    spaceBetween: 10,
    navigation: {
        nextEl: ".swiper-thumbnail-perspectiva-button-next",
        prevEl: ".swiper-thumbnail-perspectiva-button-prev",
    },
    thumbs: {
        swiper: swpPerspectiva,
    },
});

let swpLazer = new Swiper(".js-thumbnail-lazer-empreendimento", {
    direction: 'vertical',
    slidesPerView: 3,
    watchSlidesProgress: true,
});
let swpLazerThumb = new Swiper(".js-lazer-cempreendimento", {
    loop: false,
    navigation: {
        nextEl: ".swiper-thumbnail-lazer-button-next",
        prevEl: ".swiper-thumbnail-lazer-button-prev",
    },
    thumbs: {
        swiper: swpLazer,
    },
});


/**
 * EMPREENDIMENTO PLANTAS
 */

var swpPlants = new Swiper(".js-thumbnail-plants-empreendimento", {
    // loop: true,
    slidesPerView: 4,
    freeMode: true,
    watchSlidesProgress: true,
});
var swpPlantsThumb = new Swiper(".js-plants-empreendimento", {
    navigation: {
        nextEl: ".swiper-thumbnail-plants-button-next",
        prevEl: ".swiper-thumbnail-plants-button-prev",
    },
    thumbs: {
        swiper: swpPlants,
    },
});


export {
    Carousel
}