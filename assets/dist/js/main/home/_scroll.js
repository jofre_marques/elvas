let Numbers = function(){

    //Função para número dinâmicos na home
    this.counter = function($loc, $max, $time, $x, $att){
        
        //função para criar o loop de contagem
        let count = function($loc, $max, $time, $x = 0, $att){
            if($x <= $max){
                setTimeout(()=>{
                    if($max > 500 && $att < $max){
                        $x += $att
                        $loc.innerText = $x; 
                    }else{
                        $loc.innerText = $x++;
                    }
                    
                    count($loc, $max, $time, $x, $att)
                },$time)
            }
            
        }
        count($loc, $max, $time, $x, $att);
    }
}

let cmd = new Numbers();
let bool = false
//Chamando todos os objetos de contagem que existirem;

var execute = function(){
    let y = new Array();

    if(bool == false){
        let rout = document.querySelectorAll('[data-counter]');

        for(let itens of rout){
            y = itens.dataset.counter
            if(y < 50){
                cmd.counter(itens, y, 55)
            }else if(y > 50 && y <= 100){
                cmd.counter(itens, y, 20)
            }
            else if(y > 3000){
                cmd.counter(itens, y-1000, 10, 0, 1000)
            }else if(y > 1000 && y <= 3000){
                cmd.counter(itens, y-10, 5, 0, 20)
            }else{
                cmd.counter(itens, y, 30, 0, 20)
            }
        }
        bool = true
    }
}

if(document.querySelector('[data-counter]')){
    if (window.innerWidth < 600 ) {
        execute();
    }
    console.log(document.querySelector('.page-home:first-of-type .elvas_development-list'))
    document.querySelector('.page-home:first-of-type .elvas_development-list').onmouseenter = execute;
    document.querySelector('.page-home:first-of-type .elvas_development-list').onmouseout = execute;  

}


export {
    Numbers
}