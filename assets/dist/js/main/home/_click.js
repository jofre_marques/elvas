const Click = function () {

    this.barHidden = function () {
        const btn = document.querySelector(".js-btn-bar")

        setTimeout(()=>{
            let content = btn.closest(".js-content-bar")

            content.classList.toggle("hidden")
        }, 3500)

        btn.onclick = function () {
            let content = btn.closest(".js-content-bar")

            content.classList.toggle("hidden")

            if (content.classList.contains('hidden')) {
                let thumb = new Swiper('.js-swiper-main-thumbnail', {
                    spaceBetween: 10,
                    slidesPerView: 1,
                    loop: true,
                    watchSlidesProgress: true,
                    autoplay: {
                        delay: 2500,
                    },
                    navigation: {
                        nextEl: ".swiper-thumbnail-button-next",
                        prevEl: ".swiper-thumbnail-button-prev",
                    }

                })

                let bannerMain = new Swiper('.js-swiper-main', {
                    spaceBetween: 10,
                    effect: "fade",
                    loop: true,
                    autoplay: {
                        delay: 2500,
                    },
                    thumbs: {
                        swiper: thumb,
                    },
                })
            }else{
                let thumb = new Swiper('.js-swiper-main-thumbnail', {
                    spaceBetween: 10,
                    slidesPerView: 3,
                    loop: true,
                    watchSlidesProgress: true,
                    autoplay: {
                        delay: 2500,
                    },
                    navigation: {
                        nextEl: ".swiper-thumbnail-button-next",
                        prevEl: ".swiper-thumbnail-button-prev",
                    }
                
                })
                
                let bannerMain = new Swiper('.js-swiper-main', {
                    spaceBetween: 10,
                    effect: "fade",
                    loop: true,
                    autoplay: {
                        delay: 2500,
                    },
                    thumbs: {
                        swiper: thumb,
                    },
                })
            }
        }
    }
}

let cmd = new Click();

document.addEventListener("DOMContentLoaded", () => {
    if (document.querySelector('.js-btn-bar')) {
        cmd.barHidden()
    }
})


export {
    Click
}

