document.addEventListener("DOMContentLoaded", () => {
    let btn = document.querySelector(".menu-toggle")

    btn.onclick = function () {

        let menu = document.querySelector("ul#primary-menu")

        console.log(menu)
        let height = document.defaultView.getComputedStyle(menu.querySelectorAll("li")[0], null).getPropertyValue("height")

   
        if(parseFloat(menu.style.height.replace("px", "")) > 0 ){
            menu.style.height = "0px";

        }else{
            menu.style.height = ((parseFloat(height.replace("px", "")) * menu.querySelectorAll("li").length ) + 10) + "px";
        }
    }
})