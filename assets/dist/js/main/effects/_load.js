
document.addEventListener("DOMContentLoaded", ()=>{
    if(document.querySelector("[data-progress]")){
        let  $section = document.querySelectorAll('section')
       
        for(let contains of $section){
            contains.onmouseenter = function(){
                let $progress = contains.querySelectorAll("[data-progress]")
                for(let item of $progress){
                    let value = item.dataset.progress
                 
                    if(contains.querySelector('[data-barValue]').innerText == "00%"){
                        let x = 0;
                        let r = setInterval(()=>{
                            if(x <= value){
                                item.style = "clip-path: polygon(0 0, "+x+"% 0, "+x+"% 100%, 0 100%); background-color: #CEA547;";
                                let $divPai = item.closest(".js-bar")
                                $divPai.querySelector("[data-barValue]").innerText = x+"%"
                                x++;
                            }else{
                                clearInterval(r);
                            }
                        }, 100)
                    }
                    
                }
            }
        
        }
    }
});

