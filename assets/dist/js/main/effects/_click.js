const Click = function(){

    this.password = ()=>{
        const passwords = document.querySelectorAll("[data-password]")

        for(let password of passwords){
            let input = password.querySelector('input')
            let eye = password.querySelector('.js-eye')

            eye.onclick = ()=>{

                if(input.type == 'password'){
                    input.type = 'text';
                    eye.querySelector("img").src = eye.querySelector("img").src.replace("close", "open") 
                }else{
                    input.type = 'password';
                    eye.querySelector("img").src = eye.querySelector("img").src.replace("open", "close") 
                }
            }
        
        }
    }
}

let cmd = new Click();

if(document.querySelector("[data-password]")){
    cmd.password()
}


export{
    Click
}