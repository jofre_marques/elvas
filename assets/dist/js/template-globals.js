import * as internitOwlCarouselScript from './main/carousel/_carousel.js';
import * as internitMaskScript from './main/mask/_mask.js';
import * as InternitHeader from './main/header/_navtoggle.js';
import * as InternitHomeClick from './main/home/_click.js';
import * as InternitHomeScroll from './main/home/_scroll.js';
import * as InternitEffectsLoad from './main/effects/_load.js';
import * as InternitEffectClick from './main/effects/_click.js';